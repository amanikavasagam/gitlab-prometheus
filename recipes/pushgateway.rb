include_recipe "gitlab-prometheus::default"

node.default["ark"]["package_dependencies"] = []
include_recipe "ark::default"

dir_name = ::File.basename(node["pushgateway"]["dir"])
dir_path = ::File.dirname(node["pushgateway"]["dir"])

ark dir_name do
  url node["pushgateway"]["url"]
  checksum node["pushgateway"]["checksum"]
  version node["pushgateway"]["version"]
  prefix_root Chef::Config["file_cache_path"]
  path dir_path
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  action :put
  notifies :restart, "poise_service[pushgateway]", :delayed
end

poise_service "pushgateway" do
  command File.join(node["pushgateway"]["dir"], "pushgateway")
  user node["prometheus"]["user"]
  action %i(enable start)
end
