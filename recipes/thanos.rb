require "yaml"
include_recipe "gitlab-prometheus::default"

directory node["thanos"]["dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

ark ::File.basename(node["thanos"]["dir"]) do
  url node["thanos"]["binary_url"]
  checksum node["thanos"]["checksum"]
  version node["thanos"]["version"]
  prefix_root Chef::Config["file_cache_path"]
  path ::File.dirname(node["thanos"]["dir"])
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  action :put
end

# Fetch secrets from gitlab_secrets
unless node["thanos"]["gcs-creds"]["backend"].nil?
  secrets = node["thanos"]["gcs-creds"]
  google_json_data = get_secrets(secrets["backend"],
                               secrets["path"],
                               secrets["key"])
  gcs_creds = Base64.decode64(google_json_data["thanos"]["gcs-creds"]["json_base64"])

  file node["thanos"]["gcs-creds"]["file"] do
    owner node["prometheus"]["user"]
    group node["prometheus"]["group"]
    mode "0600"
    content gcs_creds
  end
end

thanos_store_search = node["thanos"]["thanos_store_search"]
thanos_store_query = search(:node, thanos_store_search).sort! { |a, b| a[:fqdn] <=> b[:fqdn] }
file node["thanos"]["store_inventory"] do
  content generate_inventory_file(thanos_store_query, node["thanos"]["cluster_port"], []).to_yaml
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0644"
end

include_recipe "runit::default"

directory node["thanos-sidecar"]["log_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
  only_if { node["thanos-sidecar"]["enable"] }
end

thanos_search = node["thanos"]["cluster_search"]
thanos_cluster_port = node["thanos"]["cluster_port"]
node.default["thanos"]["peers"] = search(:node, thanos_search).map { |n|
  [n["ipaddress"], thanos_cluster_port].join(":")
}.sort.uniq

thanos_storage_config = {
  "type" => node["thanos"]["storage"]["type"],
  "config" => node["thanos"]["storage"]["config"],
}

if node["thanos"]["storage"]["enable"]
  node.default["thanos-sidecar"]["flags"]["objstore.config-file"] = node["thanos"]["storage"]["config-file"]
  node.default["thanos-store"]["flags"]["objstore.config-file"] = node["thanos"]["storage"]["config-file"]
  node.default["thanos-compact"]["flags"]["objstore.config-file"] = node["thanos"]["storage"]["config-file"]
end

file "object-storage-config" do
  path node["thanos"]["storage"]["config-file"]
  content hash_to_yaml(thanos_storage_config)
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0644"
  only_if { node["thanos"]["storage"]["enable"] }
end

# Disable Prometheus compaction when storage uploading is enabled.
if node["thanos"]["storage"]["enable"]
  node.override["prometheus"]["flags"]["storage.tsdb.min-block-duration"] = "2h"
  node.override["prometheus"]["flags"]["storage.tsdb.max-block-duration"] = "2h"
end

runit_service "thanos-sidecar" do
  options(
    thanos_mode: "sidecar"
  )
  run_template_name "thanos"
  default_logger true
  sv_timeout 120 # Bumping from the default 7 seconds
  log_dir node["thanos-sidecar"]["log_dir"]
  subscribes :restart, "ark[thanos]", :delayed
  subscribes :restart, "file[object-storage-config]", :delayed
  only_if { node["thanos-sidecar"]["enable"] }
end

directory node["thanos-query"]["log_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
  only_if { node["thanos-query"]["enable"] }
end

runit_service "thanos-query" do
  options(
    thanos_mode: "query"
  )
  run_template_name "thanos"
  default_logger true
  sv_timeout 120 # Bumping from the default 7 seconds
  log_dir node["thanos-query"]["log_dir"]
  subscribes :restart, "ark[thanos]", :delayed
  only_if { node["thanos-query"]["enable"] }
end

directory node["thanos-store"]["log_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
  only_if { node["thanos-store"]["enable"] }
end

runit_service "thanos-store" do
  options(
    thanos_mode: "store"
  )
  run_template_name "thanos"
  default_logger true
  sv_timeout 120 # Bumping from the default 7 seconds
  log_dir node["thanos-store"]["log_dir"]
  subscribes :restart, "ark[thanos]", :delayed
  subscribes :restart, "file[object-storage-config]", :delayed
  only_if { node["thanos-store"]["enable"] }
end

directory node["thanos-compact"]["log_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
  only_if { node["thanos-compact"]["enable"] }
end

runit_service "thanos-compact" do
  options(
    thanos_mode: "compact"
  )
  run_template_name "thanos"
  default_logger true
  sv_timeout 120 # Bumping from the default 7 seconds
  log_dir node["thanos-compact"]["log_dir"]
  subscribes :restart, "ark[thanos]", :delayed
  subscribes :restart, "file[object-storage-config]", :delayed
  only_if { node["thanos-compact"]["enable"] }
end
