#
# Cookbook Name GitLab::Monitoring
# Attributes:: prometheus
#
default["prometheus"]["version"]     = "2.7.1"
default["prometheus"]["checksum"]    = "7837cfd6b5af63c07cadfa2b3118f0a43e43e8a6e6d596425cb23b45855b7db0"

default["prometheus"]["dir"]         = "/opt/prometheus/prometheus"
default["prometheus"]["alerting_rules_dir"] = "#{node['prometheus']['dir']}/alerts"
default["prometheus"]["recording_rules_dir"] = "#{node['prometheus']['dir']}/recordings"
default["prometheus"]["rules_dir"] = "#{node['prometheus']['dir']}/rules"
default["prometheus"]["console_templates_dir"] = "#{node['prometheus']['dir']}/consoles"
default["prometheus"]["inventory_dir"] = "#{node['prometheus']['dir']}/inventory"
default["prometheus"]["binary"]      = "#{node['prometheus']['dir']}/prometheus"
default["prometheus"]["log_dir"]     = "/var/log/prometheus/prometheus"
default["prometheus"]["binary_url"]  = "https://github.com/prometheus/prometheus/releases/download/v#{node['prometheus']['version']}/prometheus-#{node['prometheus']['version']}.linux-amd64.tar.gz"

default["prometheus"]["scrape_interval"] = "15s"
default["prometheus"]["scrape_timeout"] = "10s"
default["prometheus"]["evaluation_interval"] = "15s"
default["prometheus"]["external_labels"] = {}

default["prometheus"]["alertmanager"]["port"]            = "9093"
default["prometheus"]["alertmanager"]["inventory"]       = "#{node['prometheus']['dir']}/alertmanagers.yml"

default["prometheus"]["runbooks"]["git_http"]            = "https://ops.gitlab.net/gitlab-com/runbooks.git"
default["prometheus"]["runbooks"]["branch"]              = "master"

default["prometheus"]["flags"]["config.file"]           = "#{node['prometheus']['dir']}/prometheus.yml"
default["prometheus"]["flags"]["web.console.libraries"] = "#{node['prometheus']['dir']}/console_libraries"
default["prometheus"]["flags"]["web.console.templates"] = "#{node['prometheus']['dir']}/consoles"
default["prometheus"]["flags"]["web.enable-admin-api"]  = true
default["prometheus"]["flags"]["web.enable-lifecycle"]  = true
default["prometheus"]["flags"]["web.external-url"]      = "https://#{node['fqdn']}"

default["prometheus"]["flags"]["storage.tsdb.path"]      = "#{node['prometheus']['dir']}/data"
default["prometheus"]["flags"]["storage.tsdb.retention"] = "365d"
default["prometheus"]["flags"]["storage.tsdb.max-block-duration"] = "7d"

default["prometheus"]["flags"]["query.max-samples"]                = "10000000"
default["prometheus"]["flags"]["storage.remote.read-sample-limit"] = "10000000"

default["prometheus"]["rule_files"] = [
  File.join(node["prometheus"]["rules_dir"], "/*.yml"),
]

default["prometheus"]["install_method"] = "binary"

default["prometheus"]["jobs"] = []
