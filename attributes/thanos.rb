default["thanos"]["version"] = "0.3.2"
default["thanos"]["checksum"] = "f8ddd7f9c4527826b327d0ec66b07000f402e4eec5aaa1d32706438959b7bee7"
default["thanos"]["binary_url"] = "https://github.com/improbable-eng/thanos/releases/download/v#{node['thanos']['version']}/thanos-#{node['thanos']['version']}.linux-amd64.tar.gz"

default["thanos"]["dir"] = "/opt/prometheus/thanos"
default["thanos"]["binary"] = "#{default['thanos']['dir']}/thanos"
default["thanos"]["grpc-address"] = "0.0.0.0:10901"
default["thanos"]["http-address"] = "0.0.0.0:10902"
default["thanos"]["cluster_port"] = "10900"
default["thanos"]["cluster-address"] = "0.0.0.0:#{node['thanos']['cluster_port']}"
default["thanos"]["cluster_search"] = "recipes:gitlab-prometheus\\:\\:thanos"
default["thanos"]["peers"] = []
default["thanos"]["store_inventory"] = "#{default['thanos']['dir']}/thanos_store.yml"
default["thanos"]["thanos_store_search"] = "recipes:gitlab-prometheus\\:\\:thanos AND (thanos-sidecar_enable:true OR thanos-store_enable:true)"

default["thanos"]["gcs-creds"]["backend"] = nil
default["thanos"]["gcs-creds"]["path"] = "thanos"
default["thanos"]["gcs-creds"]["file"] = "#{default['thanos']['dir']}/gcs-creds.json"

default["thanos"]["storage"]["enable"] = false
default["thanos"]["storage"]["config-file"] = "#{node['thanos']['dir']}/objstore.yml"
default["thanos"]["storage"]["type"] = "GCS"
default["thanos"]["storage"]["config"] = {}

default["thanos-sidecar"]["enable"] = false
default["thanos-sidecar"]["log_dir"] = "/var/log/prometheus/thanos-sidecar"
default["thanos-sidecar"]["flags"]["prometheus.url"] = "http://localhost:9090"
default["thanos-sidecar"]["flags"]["tsdb.path"] = node["prometheus"]["flags"]["storage.tsdb.path"]
default["thanos-sidecar"]["flags"]["cluster.address"] = node["thanos"]["cluster-address"]
default["thanos-sidecar"]["flags"]["http-address"] = node["thanos"]["http-address"]
default["thanos-sidecar"]["flags"]["grpc-address"] = node["thanos"]["grpc-address"]
default["thanos-sidecar"]["flags"]["shipper.upload-compacted"] = true

default["thanos-query"]["enable"] = false
default["thanos-query"]["log_dir"] = "/var/log/prometheus/thanos-query"
default["thanos-query"]["flags"]["http-address"] = node["thanos"]["http-address"]
default["thanos-query"]["flags"]["query.replica-label"] = "replica"
default["thanos-query"]["flags"]["cluster.address"] = node["thanos"]["cluster-address"]
default["thanos-query"]["flags"]["http-address"] = node["thanos"]["http-address"]
default["thanos-query"]["flags"]["grpc-address"] = node["thanos"]["grpc-address"]

default["thanos-store"]["enable"] = false
default["thanos-store"]["log_dir"] = "/var/log/prometheus/thanos-store"
default["thanos-store"]["flags"]["data-dir"] = "#{node['thanos']['dir']}/store-data"
default["thanos-store"]["flags"]["cluster.address"] = node["thanos"]["cluster-address"]
default["thanos-store"]["flags"]["http-address"] = node["thanos"]["http-address"]
default["thanos-store"]["flags"]["grpc-address"] = node["thanos"]["grpc-address"]
default["thanos-store"]["flags"]["index-cache-size"] = (node["memory"]["total"].to_i * 0.10).to_i.to_s + "KB"
default["thanos-store"]["flags"]["chunk-pool-size"] = (node["memory"]["total"].to_i * 0.50).to_i.to_s + "KB"

default["thanos-compact"]["enable"] = false
default["thanos-compact"]["log_dir"] = "/var/log/prometheus/thanos-compact"
default["thanos-compact"]["flags"]["data-dir"] = "#{node['thanos']['dir']}/compact-data"
default["thanos-compact"]["flags"]["cluster.address"] = node["thanos"]["cluster-address"]
default["thanos-compact"]["flags"]["http-address"] = node["thanos"]["http-address"]
default["thanos-compact"]["flags"]["grpc-address"] = node["thanos"]["grpc-address"]
