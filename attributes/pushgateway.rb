default["pushgateway"]["dir"] = "/opt/prometheus/pushgateway"
default["pushgateway"]["binary"] = "#{node['pushgateway']['dir']}/pushgateway"
default["pushgateway"]["log_dir"] = "/var/log/prometheus/pushgateway"

default["pushgateway"]["version"] = "0.7.0"
default["pushgateway"]["checksum"] = "902849c94dc275f157899f7fee1b2f23efbd3bbdb6c3a3c42e503f4439f74ed2"
default["pushgateway"]["url"] = "https://github.com/prometheus/pushgateway/releases/download/v#{node['pushgateway']['version']}/pushgateway-#{node['pushgateway']['version']}.linux-amd64.tar.gz"
