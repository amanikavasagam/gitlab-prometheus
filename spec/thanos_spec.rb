require "spec_helper"
require "chef-vault"
require "chef-vault/test_fixtures"

object_storage_config = <<-OBJSTORE
---
type: GCS
config:
  bucket: I-Has-A-Bucket
OBJSTORE

describe "gitlab-prometheus::thanos" do
  include ChefVault::TestFixtures.rspec_shared_context

  context "default execution" do
    before do
      stub_search(:node, "recipes:gitlab-prometheus\\:\\:thanos").and_return(
        [
          { "fqdn" => "fist_node",
            "hostname" => "my.hostname",
            "ipaddress" => "10.0.0.1" },
          { "fqdn" => "fist_node",
            "hostname" => "labeled.hostname",
            "ipaddress" => "10.0.0.2" },
        ]
      )
      stub_search(:node, "recipes:gitlab-prometheus\\:\\:thanos AND (thanos-sidecar_enable:true OR thanos-store_enable:true)").and_return(
        [
          { "fqdn" => "fist_node",
            "hostname" => "my.hostname",
            "ipaddress" => "10.0.0.1" },
          { "fqdn" => "fist_node",
            "hostname" => "labeled.hostname",
            "ipaddress" => "10.0.0.2" },
        ]
      )
    end

    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
        node.override["thanos-sidecar"]["enable"] = true
        node.override["thanos-query"]["enable"] = true
        node.override["thanos-store"]["enable"] = true
        node.override["thanos-compact"]["enable"] = true
        node.override["thanos"]["gcs-creds"]["backend"] = "chef_vault"
        node.override["thanos"]["storage"]["enable"] = true
        node.override["thanos"]["storage"]["config"] = { "bucket" => "I-Has-A-Bucket" }
      }.converge(described_recipe)
    end

    it "creates the log dirs in the configured location" do
      expect(chef_run).to create_directory("/var/log/prometheus").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
      expect(chef_run).to create_directory("/var/log/prometheus/thanos-sidecar").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
      expect(chef_run).to create_directory("/var/log/prometheus/thanos-query").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
      expect(chef_run).to create_directory("/var/log/prometheus/thanos-store").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
      expect(chef_run).to create_directory("/var/log/prometheus/thanos-compact").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
    end

    it "creates the thanos base directory" do
      expect(chef_run).to create_directory("/opt/prometheus/thanos").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
    end

    it "creates a bucket config" do
      expect(chef_run).to render_file("/opt/prometheus/thanos/objstore.yml").with_content(object_storage_config)
    end

    it "creates a gcs creds config" do
      expect(chef_run).to render_file("/opt/prometheus/thanos/gcs-creds.json")
    end

    it "creates a bucket config" do
      expect(chef_run).to render_file("/opt/prometheus/thanos/thanos_store.yml")
    end

    let(:node) { chef_run.node }

    it "populates a cluster peers list" do
      expect(node["thanos"]["peers"]).to eq(["10.0.0.1:10900", "10.0.0.2:10900"])
    end

    it "includes runit::default" do
      expect(chef_run).to include_recipe("runit::default")
    end

    it "runs the thanos services" do
      expect(chef_run).to enable_runit_service("thanos-sidecar").with(
        default_logger: true,
        log_dir: "/var/log/prometheus/thanos-sidecar"
      )
      expect(chef_run).to enable_runit_service("thanos-query").with(
        default_logger: true,
        log_dir: "/var/log/prometheus/thanos-query"
      )
      expect(chef_run).to enable_runit_service("thanos-store").with(
        default_logger: true,
        log_dir: "/var/log/prometheus/thanos-store"
      )
      expect(chef_run).to enable_runit_service("thanos-compact").with(
        default_logger: true,
        log_dir: "/var/log/prometheus/thanos-compact"
      )
    end
  end
end
