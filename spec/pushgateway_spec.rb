require "spec_helper"

describe "gitlab-prometheus::pushgateway" do
  context "default execution" do
    cached(:chef_run) do
      ChefSpec::ServerRunner.new { |node|
      }.converge(described_recipe)
    end

    it "includes the `ark` recipe" do
      expect(chef_run).to include_recipe("ark::default")
    end

    it "enables pushgateway service" do
      expect(chef_run).to enable_poise_service("pushgateway")
    end
  end
end
