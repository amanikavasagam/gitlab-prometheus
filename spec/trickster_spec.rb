require "spec_helper"

trickster_default_config = <<-DEFAULT_TRICKSTER_CONFIG
[main]
# instance_id allows you to run multiple trickster processes on the same host and log to separate files
# Useful for baremetal, not so much for elastic deployments, so only uncomment if you really need it
#instance_id = 1

# Configuration options for the Proxy Server
[proxy_server]
# listen_port defines the port on which Trickster's Proxy server listens.
# since this is a proxy for Prometheus, we use 9090 by default, just like Prometheus does
listen_port = 9095
# listen_address defines the ip on which Trickster's Proxy server listens.
# empty by default, listening on all interfaces
listen_address = '127.0.0.1'

[cache]
# cache_type defines what kind of cache Trickster uses
# options are 'memory', 'redis' and 'filesystem'.  'memory' is the default
cache_type = 'filesystem'

# record_ttl_secs defines the relative expiration of cached queries. default is 6 hours (21600 seconds)
record_ttl_secs = 3600

# reap_sleep_ms defines how long the cache reaper waits between reap cycles. Default is 1000 (1s)
reap_sleep_ms = 1000

# compression determines whether the cache should be compressed. default is true
compression = true

    [cache.filesystem]
    cache_path = '/opt/prometheus/trickster/cache'

# Configuration options for mapping Origin(s)
[origins]
    [origins.default]
    origin_url = 'http://localhost:9090'
    api_path = '/api/v1'
    default_step = 300
    max_value_age_secs = 86400

# Configuration Options for Metrics Instrumentation
[metrics]

# listen_port defines the port that Trickster's metrics server listens on at /metrics
listen_port = 9195
# listen_address defines the ip that Trickster's metrics server listens on at /metrics
# empty by default, listening on all interfaces
listen_address = '0.0.0.0'

# Configuration Options for Logging Instrumentation
[logging]

# log_level defines the verbosity of the logger. Possible values are 'debug', 'info', 'warn', 'error'
# default is info
log_level = 'info'

# log_file defines the file location to store logs. These will be auto-rolled and maintained for you.
# not specifying a log_file (this is the default behavior) will print logs to STDOUT
#log_file = '/some/path/to/trickster.log'
DEFAULT_TRICKSTER_CONFIG

default_origins = {
  "default" => {
    "origin_url" => "http://localhost:9090",
    "api_path" => "/api/v1",
    "default_step" => 300,
    "max_value_age_secs" => 86400,
  },
}

describe "gitlab-prometheus::trickster" do
  context "default execution" do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
        node.normal["trickster"]["origins"] = default_origins
      }.converge(described_recipe)
    end

    it "creates the systemd service unit" do
      expect(chef_run).to create_systemd_unit("trickster.service")
      expect(chef_run).to enable_systemd_unit("trickster.service")
      expect(chef_run).to start_systemd_unit("trickster.service")
    end

    it "creates the configuration file with default content" do
      expect(chef_run).to create_template("/opt/prometheus/trickster/trickster.conf").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0644"
      )
      expect(chef_run).to render_file("/opt/prometheus/trickster/trickster.conf").with_content { |content|
        expect(content).to eq(trickster_default_config)
      }
    end
  end
end
